module.exports = {
    entry: [
        "./js/data/playerData.js",
        "./js/modules/eventBus.js",

    	"./js/data/items-base.js",
        "./js/data/items-prefixes.js",
        "./js/data/items-suffixes.js",
        "./js/modules/LootItem.js",
    	"./js/modules/LootGenerator.js",

        "./js/modules/ProgressBar.js",
        "./js/modules/animate.js",
        "./js/main.js"
    ],
    output: {
        path: __dirname,
        filename: "./js/app-bundle.js"
    }
};
