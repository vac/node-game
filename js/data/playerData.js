module.exports = {
	gold: 15,
	hasLife: true,
	hasMana: true,
	hasChest: true,
	upgrades: {
		miners: {
			title: 'Gold Miner',
			key: 'miners',
			gpsPer: 1,
			baseCost: 15,
			bought: 0
		},
		factories: {
			title: 'Factory',
			key: 'factories',
			gpsPer: 3,
			baseCost: 50,
			bought: 0
		},
		refineries: {
			title: 'Refinery',
			key: 'refineries',
			gpsPer: 10,
			baseCost: 500,
			bought: 0
		},
		hqmine: {
			title: 'High Quality Mine',
			key: 'hqmine',
			gpsPer: 50,
			baseCost: 3500,
			bought: 0
		}
	}
};
