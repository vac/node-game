module.exports = [
	{
		id: 'slippers',
		name: 'Slippers',
		class: 'armor',
		type: 'boots',
		reqLvl: 1,
		attributes: {
			defence: {
				min: 2,
				max: 5
			}
		}
	}, {
		id: 'breast_plate',
		name: 'Breast Plate',
		class: 'armor',
		type: 'armor',
		reqLvl: 1,
		attributes: {
			defence: {
				min: 10,
				max: 13
			}
		}
	}, {
		id: 'sandals',
		name: 'Sandals',
		class: 'armor',
		type: 'boots',
		reqLvl: 1,
		attributes: {
			defence: {
				min: 1,
				max: 3
			}
		}
	}
];
