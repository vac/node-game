module.exports = [
	{
		id: 'crude',
		name: 'Crude',
		class: 'armor',
		types: ['boots', 'armor', 'helm'],
		reqLvl: 0,
		attributes: {
			defence: {
				min: -1,
				max: -2,
				modifier: 'fixed'
			}
		}
	}, {
		id: 'sturdy',
		name: 'Sturdy',
		class: 'armor',
		types: ['boots', 'armor', 'helm'],
		reqLvl: 0,
		attributes: {
			defence: {
				min: 10,
				max: 20,
				modifier: 'percent'
			}
		}
	}, {
		id: 'godly',
		name: 'Godly',
		class: 'armor',
		types: ['armor', 'helm'],
		reqLvl: 10,
		attributes: {
			defence: {
				min: 100,
				max: 200,
				modifier: 'percent'
			}
		}
	}
];
