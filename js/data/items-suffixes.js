module.exports = [
	{
		id: 'of_vulnerability',
		name: 'of Vulnerability',
		class: 'armor',
		types: ['boots', 'armor', 'helm'],
		reqLvl: 0,
		attributes: {
			defence: {
				min: -5,
				max: -15
			}
		}
	}, {
		id: 'of_health',
		name: 'of Health',
		class: 'armor',
		types: ['boots', 'armor', 'helm'],
		reqLvl: 2,
		attributes: {
			health: {
				min: 5,
				max: 10
			}
		}
	}
];
