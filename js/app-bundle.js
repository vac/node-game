/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	__webpack_require__(2);
	__webpack_require__(3);
	__webpack_require__(4);
	__webpack_require__(5);
	__webpack_require__(6);
	__webpack_require__(7);
	__webpack_require__(8);
	module.exports = __webpack_require__(9);


/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = {
		gold: 15,
		hasLife: true,
		hasMana: true,
		hasChest: true,
		upgrades: {
			miners: {
				title: 'Gold Miner',
				key: 'miners',
				gpsPer: 1,
				baseCost: 15,
				bought: 0
			},
			factories: {
				title: 'Factory',
				key: 'factories',
				gpsPer: 3,
				baseCost: 50,
				bought: 0
			},
			refineries: {
				title: 'Refinery',
				key: 'refineries',
				gpsPer: 10,
				baseCost: 500,
				bought: 0
			},
			hqmine: {
				title: 'High Quality Mine',
				key: 'hqmine',
				gpsPer: 50,
				baseCost: 3500,
				bought: 0
			}
		}
	};


/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = [
		{
			id: 'slippers',
			name: 'Slippers',
			class: 'armor',
			type: 'boots',
			reqLvl: 1,
			attributes: {
				defence: {
					min: 2,
					max: 5
				}
			}
		}, {
			id: 'breast_plate',
			name: 'Breast Plate',
			class: 'armor',
			type: 'armor',
			reqLvl: 1,
			attributes: {
				defence: {
					min: 10,
					max: 13
				}
			}
		}, {
			id: 'sandals',
			name: 'Sandals',
			class: 'armor',
			type: 'boots',
			reqLvl: 1,
			attributes: {
				defence: {
					min: 1,
					max: 3
				}
			}
		}
	];


/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = [
		{
			id: 'crude',
			name: 'Crude',
			class: 'armor',
			types: ['boots', 'armor', 'helm'],
			reqLvl: 0,
			attributes: {
				defence: {
					min: -1,
					max: -2,
					modifier: 'fixed'
				}
			}
		}, {
			id: 'sturdy',
			name: 'Sturdy',
			class: 'armor',
			types: ['boots', 'armor', 'helm'],
			reqLvl: 0,
			attributes: {
				defence: {
					min: 10,
					max: 20,
					modifier: 'percent'
				}
			}
		}, {
			id: 'godly',
			name: 'Godly',
			class: 'armor',
			types: ['armor', 'helm'],
			reqLvl: 10,
			attributes: {
				defence: {
					min: 100,
					max: 200,
					modifier: 'percent'
				}
			}
		}
	];


/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = [
		{
			id: 'of_vulnerability',
			name: 'of Vulnerability',
			class: 'armor',
			types: ['boots', 'armor', 'helm'],
			reqLvl: 0,
			attributes: {
				defence: {
					min: -5,
					max: -15
				}
			}
		}, {
			id: 'of_health',
			name: 'of Health',
			class: 'armor',
			types: ['boots', 'armor', 'helm'],
			reqLvl: 2,
			attributes: {
				health: {
					min: 5,
					max: 10
				}
			}
		}
	];


/***/ },
/* 5 */
/***/ function(module, exports) {

	var LootItem = function(baseItem){
		baseItem = JSON.parse(JSON.stringify(baseItem));
		this.id = baseItem.id;
		this.baseName = baseItem.name;
		this.name = baseItem.name;
		this.class = baseItem.class;
		this.type = baseItem.type;
		this.reqLvl = baseItem.reqLvl;
		this.attributes = baseItem.attributes;

		this.prefixes = [];
		this.suffixes = [];
	};

	LootItem.prototype = {
		applyModsToBase: function(){
			// Apply effect
			// for (var effect in rawPrefix.attributes) {
			//     if (rawPrefix.attributes.hasOwnProperty(effect)) {

			//         var attrChange = (effect.min && effect.max) ?
			// 			randomNumberBetween(effect.min, effect.max) :
			//         	effect;

			//         if(item.attributes.hasOwnProperty(effect))
			//     }
			// }
		},
		setReqLvl: function(){
			for(var i = 0; i < this.prefixes.length; i++){
				this.reqLvl = Math.max(this.reqLvl, this.prefixes[i].reqLvl);
			}
			for(var i = 0; i < this.suffixes.length; i++){
				this.reqLvl = Math.max(this.reqLvl, this.suffixes[i].reqLvl);
			}
		},
		addPrefix: function(prefix){
			if(prefix){
				prefix = JSON.parse(JSON.stringify(prefix));
				this.prefixes.push(prefix);

				if(this.prefixes.length === 1)
					this.name = [prefix.name, this.name].join(' ');

				this.setReqLvl();
			}
			return this;
		},
		addSuffix: function(suffix){
			if(suffix){
				suffix = JSON.parse(JSON.stringify(suffix));
				this.suffixes.push(suffix);

				if(this.suffixes.length === 1)
					this.name = [this.name, suffix.name].join(' ');

				this.setReqLvl();
			}
			return this;
		}

	};

	module.exports = LootItem;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	var itemsBase = __webpack_require__(2);
	var itemsPrefixes = __webpack_require__(3);
	var itemsSuffixes = __webpack_require__(4);
	var LootItem = __webpack_require__(5);

	function randomNumberBetween(min, max){
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	function copyObj(obj){
		return JSON.parse(JSON.stringify(obj));
	}

	module.exports = {

		domNodeToData: function(el){
			return {
				lootLevel: parseInt(el.getAttribute('data-loot-level')),
				lootClass: el.getAttribute('data-loot-type'),
				dropCount: {
					min: parseInt(el.getAttribute('data-loot-drop-count-min')),
					max: parseInt(el.getAttribute('data-loot-drop-count-max'))
				}
			};
		},

		addPrefix: function(item, lootOptions){
			var availablePrefixes = itemsPrefixes.filter(function(prefix){
				var isSameType = !!~prefix.types.indexOf(item.type);
				var isSameClass = prefix.class === lootOptions.lootClass;
				var isReqLvl = prefix.reqLvl <= lootOptions.lootLevel

				return (isReqLvl && isSameClass && isSameType);
			});

			var prefixNumber = randomNumberBetween(0, availablePrefixes.length-1);
			var rawPrefix = availablePrefixes[prefixNumber];

			item.addPrefix(rawPrefix);

			return item;
		},

		addSuffix: function(item, lootOptions){
			var availableSuffixes = itemsSuffixes.filter(function(suffix){
				var isSameType = !!~suffix.types.indexOf(item.type);
				var isSameClass = suffix.class === lootOptions.lootClass;
				var isReqLvl = suffix.reqLvl <= lootOptions.lootLevel

				return (isReqLvl && isSameClass && isSameType);
			});

			var suffixNumber = randomNumberBetween(0, availableSuffixes.length-1);
			var rawSuffix = availableSuffixes[suffixNumber];

			item.addSuffix(rawSuffix);

			return item;
		},

		// Generate a single piece of loot
		generate: function(lootOptions){
			var availableItems = [];

			for(var i = 0, l = itemsBase.length; i < l; i++){
				if(itemsBase[i].reqLvl <= lootOptions.lootLevel)
					availableItems.push(itemsBase[i]);
			}

			var lootNumber = randomNumberBetween(0, availableItems.length-1);
			var baseItem = new LootItem(availableItems[lootNumber]);

			// TODO: Needs better weighted chances
			if(!(randomNumberBetween(1,5) % 5)){
				baseItem = this.addPrefix(baseItem, lootOptions);
			}
			if(!(randomNumberBetween(1,5) % 5)){
				baseItem = this.addSuffix(baseItem, lootOptions);
			}

			return baseItem;
		},

		// Return all loot generated
		getLoot: function(lootData){
			var dropcount = randomNumberBetween(lootData.dropCount.min, lootData.dropCount.max);
			var lootedItems = [];

			for(var i = 0; i < dropcount; i++){
				lootedItems.push(this.generate(lootData));
			}

			return lootedItems;
		}

	};


/***/ },
/* 7 */
/***/ function(module, exports) {

	var ProgressBar = function(el){
		var meter = document.createElement('div');
		meter.className = 'bar';

		this.container = el;
		this.meter = this.container.appendChild(meter);
		this.setProgress(this.getProgress());
	};

	ProgressBar.prototype = {
		setProgress: function(percent){
			var normalizedPercent = Math.max(0, Math.min(percent, 100));
			this.meter.style.width = normalizedPercent + '%';
			this.container.setAttribute('data-progress', normalizedPercent);
		},
		getProgress: function(){
			return +this.container.getAttribute('data-progress') || 0;
		}
	};

	module.exports = ProgressBar;


/***/ },
/* 8 */
/***/ function(module, exports) {

	function animate(settings, callback){
		var handler = settings.handler;
		var fps = settings.fps || 24;
		var length = settings.length || 0.5;

		var start = settings.start;
		var end = settings.end;
		var diff = end - start;

		// Used to keep length as precise as possible
		var lastLoop = Date.now();
		var deltaTime;

		if(diff === 0) return;

		(function _lerp(curr){
			deltaTime = Date.now() - lastLoop;

	    	var progressPercent = Math.max(0.01, Math.min(1, (curr - start) / diff));
			var easeAmt = diff / (length * fps);

			// Recalculate value
			curr = start + (diff * progressPercent) + easeAmt;
			curr = Math[diff > 0 ? 'min' : 'max'](end, curr);

			handler(parseInt(curr));

			lastLoop = Date.now();

			// Continue animation
			if(progressPercent < 1)
				setTimeout(_lerp.bind(null, curr), (1000 - deltaTime) / fps);

			// End
			else if(typeof(callback) === 'function')
	            callback();

	    })(start);
	}

	module.exports = animate;


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var eventBus = __webpack_require__(10);
	var playerData = __webpack_require__(1);
	var animate = __webpack_require__(8);
	var ProgressBar = __webpack_require__(7);
	var LootGenerator = __webpack_require__(6);

	(function(){

		// How much gold the user makes per second.
		// Recalculated when the purchase:upgrade event is fired.
		var goldPerSecond = (function(){

			var currentGps = 1;

			function calculateGPS(){
				var upgrades = playerData.upgrades;
				var gps = 1;

				for (var upKey in upgrades) {
					if (upgrades.hasOwnProperty(upKey)) {
						var upgrade = upgrades[upKey];
						gps += upgrade.bought * upgrade.gpsPer;
					}
				}

				currentGps = +gps.toFixed(2);

				document.querySelector('#per-second').innerText = currentGps;
			}

			calculateGPS();

			eventBus.subscribe('purchase:upgrade', 0, calculateGPS);

			return function(){
				return +currentGps;
			}
		})();


		var gameLoop = (function(){

			var goldCounter = document.querySelector('#gold-amount');
			var lastGoldAmount = playerData.gold;
			var lastLoopId;

			return (function _loop(){

				clearTimeout(lastLoopId);

				animate({ // this keeps going even if timeout is cleared
					start: lastGoldAmount,
					end: playerData.gold += goldPerSecond(),
					length: 1,
					handler: function(val){
						goldCounter.innerText = val;
					}
				});

				// Lets loop animate to next gold amount if it's set
				// outside this function
				lastGoldAmount = playerData.gold;

				lastLoopId = setTimeout(_loop, 1000);

				return _loop;
			})();

		})();

		function sortByKey(array, key) {
		    return array.sort(function(a, b) {
		        var x = a[key]; var y = b[key];
		        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		    });
		}

		function getActionButton(action){
			return document.querySelector('[data-action="'+action+'"]');
		}

		// Calculated the cost of the next upgrade exponentially
		function costForNextUpgrade(upgradeObject){
			return Math.round((upgradeObject.bought * 1.3) *
				upgradeObject.baseCost);
		}

		var health = new ProgressBar(document.querySelector('#health'));
		getActionButton('takeDamage').onclick = function(){
			playerData.gold += 10;

			var oldValue = health.getProgress();
			health.setProgress(oldValue - 10);

			// Animate progress bar's text
			animate({
				start:oldValue,
				end: health.getProgress(),
				handler: function(val){
					document.querySelector('#healthNumbers .current').innerText	= val;
				}
			});

			gameLoop();
		}

		var mana = new ProgressBar(document.querySelector('#mana'));
		getActionButton('drinkPotion').onclick = function(){
			if(playerData.gold < 10)
				return;

			playerData.gold -= 10;

			var oldValue = mana.getProgress();
			mana.setProgress(oldValue + 10);

			animate({
				start:oldValue,
				end: mana.getProgress(),
				handler: function(val){
					document.querySelector('#manaNumbers .current').innerText = val;
				}
			});

			gameLoop();
		}

		var energy = new ProgressBar(document.querySelector('#energy'));
		getActionButton('eatScaryFood').onclick = function(){

			var oldValue = energy.getProgress();
			energy.setProgress(Math.round (Math.random() * 100));

			animate({
				start:oldValue,
				end: energy.getProgress(),
				handler: function(val){
					document.querySelector('#energyNumbers .current').innerText = val;
				}
			});
		}

		getActionButton('gainLevel').onclick = function(){
			animate({
				start: health.getProgress(),
				end: 100,
				handler: function(val){
					document.querySelector('#healthNumbers .current').innerText	= val;
				}
			});

			animate({
				start: mana.getProgress(),
				end: 100,
				handler: function(val){
					document.querySelector('#manaNumbers .current').innerText = val;
				}
			});

			animate({
				start: energy.getProgress(),
				end: 100,
				handler: function(val){
					document.querySelector('#energyNumbers .current').innerText	= val;
				}
			});

			health.setProgress(100);
			mana.setProgress(100);
			energy.setProgress(100);

			var chestBtn = getActionButton('openChest');
			var lootLvl = parseInt(chestBtn.getAttribute('data-loot-level')) + 1;

			document.querySelector('#chest-level').innerText = lootLvl;
			chestBtn.setAttribute('data-loot-level', lootLvl);
		}

		getActionButton('openChest').onclick = function () {
			var resultNode = document.querySelector('#chest-result');
			var loot = LootGenerator.getLoot(LootGenerator.domNodeToData(this));

			// Remove all previous text
			while (resultNode.firstChild)
			    resultNode.removeChild(resultNode.firstChild);

			for(var i = 0; i < loot.length; i++){
				if(i < loot.length){ // ??
					var item = document.createElement('p');
					var recievedText = document.createTextNode('Recieved: ');
					var lootName = document.createElement('span');
					var punctuation = document.createTextNode('!');

					if(loot[i].prefixes.length > 0 || loot[i].suffixes.length > 0)
						lootName.setAttribute('class', 'item-magic');

					lootName.innerText = loot[i].name;

					item.appendChild(recievedText);
					item.appendChild(lootName);
					item.appendChild(punctuation);

					resultNode.appendChild(item);
				}
			}
		}

		// Purchase an upgrade
		document.querySelector('#upgrade-item-container').addEventListener('click', function(e){
			var node = e.target;
			var upgradeItemNode;

			// Search up the DOM for node of interest,
			// but limit the delegate scope to containing item
			while(node !== this){
				if(node.classList.contains('upgrade-item')){
					upgradeItemNode = node;
					break;
				}
				node = node.parentNode;
			}

			// Nothing of interest clicked
			if(upgradeItemNode === undefined){
				return;
			}

			var upgradeType = upgradeItemNode.getAttribute('data-upgrade');
			var upgradeData = playerData.upgrades[upgradeType];
			var amtToCharge = costForNextUpgrade(upgradeData);

			if(playerData.gold < amtToCharge){
				return;
			}

			playerData.gold -= amtToCharge;
			upgradeData.bought++;
			eventBus.notify('purchase:upgrade', upgradeType, upgradeData.bought);

			upgradeItemNode.querySelector('[data-upgrade-cost]').innerText = costForNextUpgrade(upgradeData);
			upgradeItemNode.querySelector('[data-upgrade-count]').innerText = upgradeData.bought;

			gameLoop();
	        e.stopPropagation();
		});

		function showUpgrade(upgradeObj){
			var template = document.querySelector('#upgradeItemContainer').firstChild.textContent;
			template = template
						.replace(/\{key\}/g, upgradeObj.key)
						.replace(/\{title\}/g, upgradeObj.title)
						.replace(/\{gps\}/g, upgradeObj.gpsPer)
						.replace(/\{cost\}/g, upgradeObj.baseCost)
						.replace(/\{count\}/g, upgradeObj.bought);

			var templateContainer = document.createElement('div');
			templateContainer.innerHTML = template;

			document.querySelector('#upgrade-item-container')
				.appendChild(templateContainer.children[0]);
		}

		// Show new upgrade when available
		eventBus.subscribe('purchase:upgrade', 1, function(eventType, currUpgradeAmt){
			// Only check for new upgrades on the first purchase
			if(currUpgradeAmt > 1){
				return;
			}

			var upgrades = [];

			// Get all player upgrade data
			var playerUpgrades = playerData.upgrades;
			for (var upKey in playerUpgrades) {
				if (playerUpgrades.hasOwnProperty(upKey)) {
					upgrades.push(playerUpgrades[upKey]);
				}
			}

			// sort by cost
			upgrades = sortByKey(upgrades, 'baseCost');

			// Filter out items we already see
			upgrades = upgrades.filter(function(val){
				return val.baseCost > playerUpgrades[eventType].baseCost;
			});

			if(upgrades.length === 0){
				return;
			}

			// Get first item and show it
			showUpgrade(upgrades.shift());

		});

	})();


/***/ },
/* 10 */
/***/ function(module, exports) {

	module.exports = (function(){
		var _subscriptions = {},
			_idList = {};

		function subscribe(eventName, id, callback){
			_subscriptions[eventName] = _subscriptions[eventName] || {};
			_subscriptions[eventName][id] = callback;

			_idList[eventName] = _idList[eventName] || [];
			_idList[eventName].push(id);
		}

		function unsubscribe(eventName, id){
			if(_subscriptions[eventName]){
				delete _subscriptions[eventName][id];
				_idList[eventName].splice(_idList[eventName].indexOf(id), 1);
			}
		}

		function unsubscribeAll(){
			for(var e in _subscriptions){
				if(_subscriptions.hasOwnProperty(e)){
					delete _subscriptions[e];
				}
			}
			for(var id in _idList){
				if(_idList.hasOwnProperty(id)){
					delete _idList[e];
				}
			}
			_subscriptions = {};
			_idList = {};
		}

		function notify(eventName){
			if(!_idList[eventName]){
				return;
			}

			// Invoke all callbacks and pass in any arguments after eventName
			for(var i = 0, l = _idList[eventName].length; i < l; i++){
				if(typeof _subscriptions[eventName][_idList[eventName][i]] === 'function'){
					_subscriptions[eventName][_idList[eventName][i]]
						.apply(null, Array.prototype.slice.call(arguments, 1));
				}
			}
		}

		function subscriberCount(eventName){
			var count = 0;
			for(var e in _subscriptions[eventName]){
				if(e !== null && e !== undefined){
					count++;
				}
			}
			return count;
		}

		return {
			subscribe: subscribe,
			unsubscribe: unsubscribe,
			unsubscribeAll: unsubscribeAll,
			notify: notify,
			subscriberCount: subscriberCount
		};
	})();


/***/ }
/******/ ]);