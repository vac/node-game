module.exports = (function(){
	var _subscriptions = {},
		_idList = {};

	function subscribe(eventName, id, callback){
		_subscriptions[eventName] = _subscriptions[eventName] || {};
		_subscriptions[eventName][id] = callback;

		_idList[eventName] = _idList[eventName] || [];
		_idList[eventName].push(id);
	}

	function unsubscribe(eventName, id){
		if(_subscriptions[eventName]){
			delete _subscriptions[eventName][id];
			_idList[eventName].splice(_idList[eventName].indexOf(id), 1);
		}
	}

	function unsubscribeAll(){
		for(var e in _subscriptions){
			if(_subscriptions.hasOwnProperty(e)){
				delete _subscriptions[e];
			}
		}
		for(var id in _idList){
			if(_idList.hasOwnProperty(id)){
				delete _idList[e];
			}
		}
		_subscriptions = {};
		_idList = {};
	}

	function notify(eventName){
		if(!_idList[eventName]){
			return;
		}

		// Invoke all callbacks and pass in any arguments after eventName
		for(var i = 0, l = _idList[eventName].length; i < l; i++){
			if(typeof _subscriptions[eventName][_idList[eventName][i]] === 'function'){
				_subscriptions[eventName][_idList[eventName][i]]
					.apply(null, Array.prototype.slice.call(arguments, 1));
			}
		}
	}

	function subscriberCount(eventName){
		var count = 0;
		for(var e in _subscriptions[eventName]){
			if(e !== null && e !== undefined){
				count++;
			}
		}
		return count;
	}

	return {
		subscribe: subscribe,
		unsubscribe: unsubscribe,
		unsubscribeAll: unsubscribeAll,
		notify: notify,
		subscriberCount: subscriberCount
	};
})();
