var ProgressBar = function(el){
	var meter = document.createElement('div');
	meter.className = 'bar';

	this.container = el;
	this.meter = this.container.appendChild(meter);
	this.setProgress(this.getProgress());
};

ProgressBar.prototype = {
	setProgress: function(percent){
		var normalizedPercent = Math.max(0, Math.min(percent, 100));
		this.meter.style.width = normalizedPercent + '%';
		this.container.setAttribute('data-progress', normalizedPercent);
	},
	getProgress: function(){
		return +this.container.getAttribute('data-progress') || 0;
	}
};

module.exports = ProgressBar;
