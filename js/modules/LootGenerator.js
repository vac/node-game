var itemsBase = require('../data/items-base.js');
var itemsPrefixes = require('../data/items-prefixes.js');
var itemsSuffixes = require('../data/items-suffixes.js');
var LootItem = require('../modules/LootItem.js');

function randomNumberBetween(min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function copyObj(obj){
	return JSON.parse(JSON.stringify(obj));
}

module.exports = {

	domNodeToData: function(el){
		return {
			lootLevel: parseInt(el.getAttribute('data-loot-level')),
			lootClass: el.getAttribute('data-loot-type'),
			dropCount: {
				min: parseInt(el.getAttribute('data-loot-drop-count-min')),
				max: parseInt(el.getAttribute('data-loot-drop-count-max'))
			}
		};
	},

	addPrefix: function(item, lootOptions){
		var availablePrefixes = itemsPrefixes.filter(function(prefix){
			var isSameType = !!~prefix.types.indexOf(item.type);
			var isSameClass = prefix.class === lootOptions.lootClass;
			var isReqLvl = prefix.reqLvl <= lootOptions.lootLevel

			return (isReqLvl && isSameClass && isSameType);
		});

		var prefixNumber = randomNumberBetween(0, availablePrefixes.length-1);
		var rawPrefix = availablePrefixes[prefixNumber];

		item.addModifier(rawPrefix, item.prefixes);

		return item;
	},

	addSuffix: function(item, lootOptions){
		var availableSuffixes = itemsSuffixes.filter(function(suffix){
			var isSameType = !!~suffix.types.indexOf(item.type);
			var isSameClass = suffix.class === lootOptions.lootClass;
			var isReqLvl = suffix.reqLvl <= lootOptions.lootLevel

			return (isReqLvl && isSameClass && isSameType);
		});

		var suffixNumber = randomNumberBetween(0, availableSuffixes.length-1);
		var rawSuffix = availableSuffixes[suffixNumber];

		item.addModifier(rawSuffix, item.suffixes);

		return item;
	},

	// Generate a single piece of loot
	generate: function(lootOptions){
		var availableItems = [];

		for(var i = 0, l = itemsBase.length; i < l; i++){
			if(itemsBase[i].reqLvl <= lootOptions.lootLevel)
				availableItems.push(itemsBase[i]);
		}

		var lootNumber = randomNumberBetween(0, availableItems.length-1);
		var baseItem = new LootItem(availableItems[lootNumber]);

		// TODO: Needs better weighted chances
		if(!(randomNumberBetween(1,5) % 4)){
			baseItem = this.addPrefix(baseItem, lootOptions);
		}
		if(!(randomNumberBetween(1,5) % 4)){
			baseItem = this.addSuffix(baseItem, lootOptions);
		}

		return baseItem;
	},

	// Return all loot generated
	getLoot: function(lootData){
		var dropcount = randomNumberBetween(lootData.dropCount.min, lootData.dropCount.max);
		var lootedItems = [];

		for(var i = 0; i < dropcount; i++){
			lootedItems.push(this.generate(lootData));
		}

		return lootedItems;
	}

};
