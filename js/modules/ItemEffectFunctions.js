
/**
 * Reducer functions that apply item effects to a character.
 * @type {Object}
 */
module.exports = {
	modifyAttackSpeed: function	(target, amt) {
		target.effectiveStats.attackSpeed += target.baseStats.attackSpeed * amt;
	}
};
