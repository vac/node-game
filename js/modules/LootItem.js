var LootItem = function(baseItem){
	baseItem = JSON.parse(JSON.stringify(baseItem));
	this.id = baseItem.id;
	this.baseName = baseItem.name;
	this.name = baseItem.name;
	this.class = baseItem.class;
	this.type = baseItem.type;
	this.reqLvl = baseItem.reqLvl;
	this.attributes = baseItem.attributes;

	this.prefixes = [];
	this.suffixes = [];
};

LootItem.prototype = {
	applyModsToBase: function(){
		// Apply effect
		// for (var effect in rawPrefix.attributes) {
		//     if (rawPrefix.attributes.hasOwnProperty(effect)) {

		//         var attrChange = (effect.min && effect.max) ?
		// 			randomNumberBetween(effect.min, effect.max) :
		//         	effect;

		//         if(item.attributes.hasOwnProperty(effect))
		//     }
		// }
	},
	setReqLvl: function(){
		for(var i = 0; i < this.prefixes.length; i++){
			this.reqLvl = Math.max(this.reqLvl, this.prefixes[i].reqLvl);
		}
		for(var i = 0; i < this.suffixes.length; i++){
			this.reqLvl = Math.max(this.reqLvl, this.suffixes[i].reqLvl);
		}
	},
	/**
	 * Add a modifier to an item.
	 * @param Modifier 	mod 			Raw modifier object from the list of
	 *                       	 		prefixes and suffixes.
	 * @param Array 	modDestination 	Prefix or Suffix array to push the
	 *                               	modifier to.
	 */
	addModifier: function(mod, modDestination){
		if(mod){
			mod = JSON.parse(JSON.stringify(mod)); // Easy deep copy
			modDestination.push(mod);

			this.setReqLvl();
			this.setName();
		}
		return this;
	},
	setName: function() {
		this.name = [
			this.prefixes.length > 0 ? this.prefixes[0].name : '',
			this.baseName,
			this.suffixes.length > 0 ? this.suffixes[0].name : ''
		].join(' ').trim();
	}
};

module.exports = LootItem;
