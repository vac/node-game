function animate(settings, callback){
	var handler = settings.handler;
	var fps = settings.fps || 24;
	var length = settings.length || 0.5;

	var start = settings.start;
	var end = settings.end;
	var diff = end - start;

	// Used to keep length as precise as possible
	var lastLoop = Date.now();
	var deltaTime;

	if(diff === 0) return;

	(function _lerp(curr){
		deltaTime = Date.now() - lastLoop;

    	var progressPercent = Math.max(0.01, Math.min(1, (curr - start) / diff));
		var easeAmt = diff / (length * fps);

		// Recalculate value
		curr = start + (diff * progressPercent) + easeAmt;
		curr = Math[diff > 0 ? 'min' : 'max'](end, curr);

		handler(parseInt(curr));

		lastLoop = Date.now();

		// Continue animation
		if(progressPercent < 1)
			setTimeout(_lerp.bind(null, curr), (1000 - deltaTime) / fps);

		// End
		else if(typeof(callback) === 'function')
            callback();

    })(start);
}

module.exports = animate;
