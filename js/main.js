var eventBus = require('./modules/eventBus.js');
var playerData = require('./data/playerData.js');
var animate = require('./modules/animate.js');
var ProgressBar = require('./modules/ProgressBar.js');
var LootGenerator = require('./modules/LootGenerator.js');

(function(){

	// How much gold the user makes per second.
	// Recalculated when the purchase:upgrade event is fired.
	var goldPerSecond = (function(){

		var currentGps = 1;

		function calculateGPS(){
			var upgrades = playerData.upgrades;
			var gps = 1;

			for (var upKey in upgrades) {
				if (upgrades.hasOwnProperty(upKey)) {
					var upgrade = upgrades[upKey];
					gps += upgrade.bought * upgrade.gpsPer;
				}
			}

			currentGps = +gps.toFixed(2);

			document.querySelector('#per-second').innerText = currentGps;
		}

		calculateGPS();

		eventBus.subscribe('purchase:upgrade', 0, calculateGPS);

		return function(){
			return +currentGps;
		}
	})();


	var gameLoop = (function(){

		var goldCounter = document.querySelector('#gold-amount');
		var lastGoldAmount = playerData.gold;
		var lastLoopId;

		return (function _loop(){

			clearTimeout(lastLoopId);

			animate({ // this keeps going even if timeout is cleared
				start: lastGoldAmount,
				end: playerData.gold += goldPerSecond(),
				length: 1,
				handler: function(val){
					goldCounter.innerText = val;
				}
			});

			// Lets loop animate to next gold amount if it's set
			// outside this function
			lastGoldAmount = playerData.gold;

			lastLoopId = setTimeout(_loop, 1000);

			return _loop;
		})();

	})();

	function sortByKey(array, key) {
	    return array.sort(function(a, b) {
	        var x = a[key]; var y = b[key];
	        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	    });
	}

	function getActionButton(action){
		return document.querySelector('[data-action="'+action+'"]');
	}

	// Calculated the cost of the next upgrade exponentially
	function costForNextUpgrade(upgradeObject){
		return Math.round((upgradeObject.bought * 1.3) *
			upgradeObject.baseCost);
	}

	var health = new ProgressBar(document.querySelector('#health'));
	getActionButton('takeDamage').onclick = function(){
		playerData.gold += 10;

		var oldValue = health.getProgress();
		health.setProgress(oldValue - 10);

		// Animate progress bar's text
		animate({
			start:oldValue,
			end: health.getProgress(),
			handler: function(val){
				document.querySelector('#healthNumbers .current').innerText	= val;
			}
		});

		gameLoop();
	}

	var mana = new ProgressBar(document.querySelector('#mana'));
	getActionButton('drinkPotion').onclick = function(){
		if(playerData.gold < 10)
			return;

		playerData.gold -= 10;

		var oldValue = mana.getProgress();
		mana.setProgress(oldValue + 10);

		animate({
			start:oldValue,
			end: mana.getProgress(),
			handler: function(val){
				document.querySelector('#manaNumbers .current').innerText = val;
			}
		});

		gameLoop();
	}

	var energy = new ProgressBar(document.querySelector('#energy'));
	getActionButton('eatScaryFood').onclick = function(){

		var oldValue = energy.getProgress();
		energy.setProgress(Math.round (Math.random() * 100));

		animate({
			start:oldValue,
			end: energy.getProgress(),
			handler: function(val){
				document.querySelector('#energyNumbers .current').innerText = val;
			}
		});
	}

	getActionButton('gainLevel').onclick = function(){
		animate({
			start: health.getProgress(),
			end: 100,
			handler: function(val){
				document.querySelector('#healthNumbers .current').innerText	= val;
			}
		});

		animate({
			start: mana.getProgress(),
			end: 100,
			handler: function(val){
				document.querySelector('#manaNumbers .current').innerText = val;
			}
		});

		animate({
			start: energy.getProgress(),
			end: 100,
			handler: function(val){
				document.querySelector('#energyNumbers .current').innerText	= val;
			}
		});

		health.setProgress(100);
		mana.setProgress(100);
		energy.setProgress(100);

		var chestBtn = getActionButton('openChest');
		var lootLvl = parseInt(chestBtn.getAttribute('data-loot-level')) + 1;

		document.querySelector('#chest-level').innerText = lootLvl;
		chestBtn.setAttribute('data-loot-level', lootLvl);
	}

	getActionButton('openChest').onclick = function () {
		var resultNode = document.querySelector('#chest-result');
		var loot = LootGenerator.getLoot(LootGenerator.domNodeToData(this));

		// Remove all previous text
		while (resultNode.firstChild)
		    resultNode.removeChild(resultNode.firstChild);

		for(var i = 0; i < loot.length; i++){
			if(i < loot.length){ // ??
				var item = document.createElement('p');
				var recievedText = document.createTextNode('Recieved: ');
				var lootName = document.createElement('span');
				var punctuation = document.createTextNode('!');

				if(loot[i].prefixes.length > 0 || loot[i].suffixes.length > 0)
					lootName.setAttribute('class', 'item-magic');

				lootName.innerText = loot[i].name;

				item.appendChild(recievedText);
				item.appendChild(lootName);
				item.appendChild(punctuation);

				resultNode.appendChild(item);
			}
		}
	}

	// Purchase an upgrade
	document.querySelector('#upgrade-item-container').addEventListener('click', function(e){
		var node = e.target;
		var upgradeItemNode;

		// Search up the DOM for node of interest,
		// but limit the delegate scope to containing item
		while(node !== this){
			if(node.classList.contains('upgrade-item')){
				upgradeItemNode = node;
				break;
			}
			node = node.parentNode;
		}

		// Nothing of interest clicked
		if(upgradeItemNode === undefined){
			return;
		}

		var upgradeType = upgradeItemNode.getAttribute('data-upgrade');
		var upgradeData = playerData.upgrades[upgradeType];
		var amtToCharge = costForNextUpgrade(upgradeData);

		if(playerData.gold < amtToCharge){
			return;
		}

		playerData.gold -= amtToCharge;
		upgradeData.bought++;
		eventBus.notify('purchase:upgrade', upgradeType, upgradeData.bought);

		upgradeItemNode.querySelector('[data-upgrade-cost]').innerText = costForNextUpgrade(upgradeData);
		upgradeItemNode.querySelector('[data-upgrade-count]').innerText = upgradeData.bought;

		gameLoop();
        e.stopPropagation();
	});

	function showUpgrade(upgradeObj){
		var template = document.querySelector('#upgradeItemContainer').firstChild.textContent;
		template = template
					.replace(/\{key\}/g, upgradeObj.key)
					.replace(/\{title\}/g, upgradeObj.title)
					.replace(/\{gps\}/g, upgradeObj.gpsPer)
					.replace(/\{cost\}/g, upgradeObj.baseCost)
					.replace(/\{count\}/g, upgradeObj.bought);

		var templateContainer = document.createElement('div');
		templateContainer.innerHTML = template;

		document.querySelector('#upgrade-item-container')
			.appendChild(templateContainer.children[0]);
	}

	// Show new upgrade when available
	eventBus.subscribe('purchase:upgrade', 1, function(eventType, currUpgradeAmt){
		// Only check for new upgrades on the first purchase
		if(currUpgradeAmt > 1){
			return;
		}

		var upgrades = [];

		// Get all player upgrade data
		var playerUpgrades = playerData.upgrades;
		for (var upKey in playerUpgrades) {
			if (playerUpgrades.hasOwnProperty(upKey)) {
				upgrades.push(playerUpgrades[upKey]);
			}
		}

		// sort by cost
		upgrades = sortByKey(upgrades, 'baseCost');

		// Filter out items we already see
		upgrades = upgrades.filter(function(val){
			return val.baseCost > playerUpgrades[eventType].baseCost;
		});

		if(upgrades.length === 0){
			return;
		}

		// Get first item and show it
		showUpgrade(upgrades.shift());

	});

})();
